import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { User } from 'firebase';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private toast: ToastController,
    private auth: AngularFireAuth
  ) {
    this.isLoggedIn = this.auth.authState;
  }

  login(user: any) {
    this.auth.signInWithEmailAndPassword(user.email, user.password)
      .then(() => this.nav.navigateForward('home'))
      .catch(() => this.showError());
  }

  recoverPass(email: string) {
    this.auth.sendPasswordResetEmail(email)
      .then(() => {
        this.showSuccess();
        this.nav.navigateBack('auth');
      })
      .catch(err => {
        this.showError();
      });
  }

  createUser(user: any) {
    this.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(() => this.showSuccessCreate());
  }

  private async showError() {
    const ctrl = await this.toast.create({
      message: "Dados de acesso incorretos",
      duration: 3000
    });

    ctrl.present();
  }

  private async showSuccessCreate() {
    const ctrl = await this.toast.create({
      message: "Usuário criado com sucesso",
      duration: 3000
    });

    ctrl.present();
  }

  
  private async showSuccess() {
    const ctrl = await this.toast.create({
      message: "E-mail de recuperação enviado!",
      duration: 3000
    });

    ctrl.present();
  }

  logout() {
    this.auth.signOut()
      .then(() => this.nav.navigateBack('auth'));
  }
}
