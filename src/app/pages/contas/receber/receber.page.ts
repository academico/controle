import { Component, OnInit } from '@angular/core';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'receber',
  templateUrl: './receber.page.html',
  styleUrls: ['./receber.page.scss'],
})
export class ReceberPage implements OnInit {
  listaContas: any;

  constructor(
    private contaService: ContaService
  ) { }

  ngOnInit() {
    this.obterContas();
  }

  obterContas() {
    this.contaService.listarConta("receber").subscribe(contas => {
      this.listaContas = contas;
    })
  }
  
  remove(conta: any) {
    this.contaService.removerConta(conta);
  }
}
