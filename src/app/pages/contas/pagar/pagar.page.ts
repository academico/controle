import { Component, OnInit } from '@angular/core';
import { ContaService } from '../service/conta.service';

@Component({
  selector: 'pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaContas: any;

  constructor(
    private contaService: ContaService
  ) { }

  ngOnInit() {
    this.obterContas();
  }

  obterContas() {
    this.contaService.listarConta("pagar").subscribe(contas => {
      this.listaContas = contas;
    })
  }
  
  remove(conta: any) {
    this.contaService.removerConta(conta);
  }
}
