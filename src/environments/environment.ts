// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBZTY78zdrC_KpI3z3rHsVUUy5hXNrIpGA',
    authDomain: 'controle-if-olavo.firebaseapp.com',
    databaseURL: 'https://controle-if-olavo.firebaseio.com',
    projectId: 'controle-if-olavo',
    storageBucket: 'controle-if-olavo.appspot.com',
    messagingSenderId: '940415513012',
    appId: '1:940415513012:web:2014eb54033231709442e1',
    measurementId: 'G-KHCGKMF21L'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
